<?php

/**
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Forms
 * @copyright GAZE-ND
 */

/**
 * Login Form
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Forms
 * @copyright GAZE-ND
 */
class Admin_Form_Login extends Zend_Form {
    
    /**
     * Add form elements
     */
	public function init() {
		/* Form Elements & Other Definitions Here ... */
		// TODO create form and form elements + checks: username,pass and submit
		// button
		$this->setMethod ( 'post' );
		
		$this->addElement ( 'text', 'username', array (
				'label' => 'Username',
				'required' => true,
				'filters' => array (
						'StringTrim' 
				) 
		) );
		$this->addElement ( 'password', 'password', array (
				'label' => 'Password',
				'required' => true 
		) );
		
		$this->addElement ( 'submit', 'submit', array (
				'ignore' => true,
				'label' => 'Log In' 
		) );
		$this->addElement ( 'hash', 'csrf', array (
				'ignore' => true 
		) );
	}
}

