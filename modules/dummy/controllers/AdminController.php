<?php
/**
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Controllers
 * @copyright GAZE-ND
 */

/**
 * Admin Controller
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Controllers
 * @copyright GAZE-ND
 */

class Dummy_AdminController extends GZend_Controller_AdminActions
{
    
    public function indexAction()
    {
        $this->view->message = "hello Piperkov, i need money :))";
    }
}
