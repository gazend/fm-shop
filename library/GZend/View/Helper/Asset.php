<?php
/**
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category GZend
 * @package GZend Helpers
 * @copyright GAZE-ND
 */

/**
 * Asset Helper
 * 
 * @uses actionHelper GZend_View_Helper
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category GZend
 * @package GZend Helpers
 * @copyright GAZE-ND
 */
class GZend_View_Helper_Asset extends Zend_View_Helper_Abstract
{
    
    /**
     * Creates Group specific asset URL
     * 
     * @param string $resource Resource Path
     * @param string $group 
     *     [Optional] Group Name. If null, it will automatically choose group
     * @return string
     */
    public function asset($resource, $group = null)
    {
        if (null === $group) {
            $request = Zend_Controller_Front::getInstance()->getRequest();
            $controller = $request->getControllerName();
            if ('admin' === $controller) {
                $group = 'backend';                
            } else {
                $group = 'frontend';
            }
        }
        
        if (!is_string($resource)) {
            throw new GZend_View_Helper_Exception('$resource must be String');
        }
                
        return $this->view->baseUrl('assets/' . $group . '/' . $resource);
    }  
    
}