<?php
/** 
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 * 
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Bootstrap
 * @copyright GAZE-ND
 */

/**
 * Applicaiton Bootstrap
 * 
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Bootstrap
 * @copyright GAZE-ND 
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
	
	/**
	 * Initialize Application AutoLoader
	 * 
	 * @return Zend_Application_Module_Autoloader
	 */	
	protected function _initApplicationAutoloader()
	{
	    $autoloader = new Zend_Application_Module_Autoloader(array(
	            'namespace' => 'Application_',
	            'basePath' => APPLICATION_PATH 
	    ));
	    return $autoloader;
	} 
	
	/**
	 * Set the HTML DocType
	 */
	protected function _initDoctype()
	{
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
	}
	
	/**
	 * Adding Application Routes
	 */
	protected function _initRoutes()
	{
	    $routesIniPath = __DIR__ . '/configs/routes.ini';
	    $config = new Zend_Config_Ini($routesIniPath);
	    $router = Zend_Controller_Front::getInstance()->getRouter();
	    $router->addConfig($config, 'routes');
	} 

}

