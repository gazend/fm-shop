<?php
/**
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */

/**
 * Mapper for "users" table 
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */ 
class Applicaion_Model_UsersMapper
{
    
    /**
     * Table data gateway
     * 
     * @var Zend_Db_Table_Abstract 
     */
    protected $gateway;

    /**
     * Constructor
     * 
     * Sets dependencies
     * 
     * @param Zend_Db_Table_Abstract $gateway
     */
    public function __construct(Zend_Db_Table_Abstract $gateway = null)
    {
        if (null === $gateway) {
            $gateway = new Zend_Db_Table('users'); 
        }
        $this->gateway = $gateway;                 
    }
    
    /**
     * Insert new row into 'users' table
     * 
     * @param Application_Model_AdminUserEntity $entity
     * @return Applicaion_Model_UsersMapper
     */
    public function insert(Application_Model_AdminUserEntity $entity)
    {
        $entity->setId(null);
        $data = $entity->toArray();
        $id = $this->gateway->insert($data);
        $entity->setId($id);
        return $this; 
    }
    
    /**
     * Updates row into 'users' table
     * 
     * @param Application_Model_AdminUserEntity $entity
     * @return Applicaion_Model_UsersMapper
     */
    public function update(Application_Model_AdminUserEntity $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }
        $where = $this->gateway->getAdapter()->quoteInto('id = ?', $id);
        $data = $entity->toArray();
        $this->gateway->update($data, $where);
        return $this;        
    }
    
    /**
     * Deletes row from 'users' table
     * 
     * @param Application_Model_AdminUserEntity $entity
     * @return Applicaion_Model_UsersMapper
     */
    public function delete(Application_Model_AdminUserEntity $entity)
    {
        $id = $entity->getId();
        if (empty($id)) {
            return $this;
        }
        $where = $this->gateway->getAdapter()->quoteInto('id = ?', $id);
        $this->gateway->delete($where);
        return $this; 
    }
}