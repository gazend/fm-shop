<?php
class Application_Form_Login extends Zend_Form
{
    const PASSWORD_MIN_LENGHT = 5;

    protected $usernameDecorators = array(
        array(array('IconWrapper' => 'HtmlTag'), array('tag' => 'i', 'class' => 'icon-user')),
        array(array('SpanWrapper' => 'HtmlTag'), array('tag' => 'span', 'class' => 'add-on')),
        array('ViewHelper', array('placement' => 'append')),
        array(array('DivWrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-prepend', 'data-rel' => 'tooltip', 'data-original-title' => 'Username')),
        array(array('ClearFix' => 'HtmlTag'), array('tag' => 'div', 'class' => 'clear-fix', 'placement' => 'append')),
        'Errors'
    );
    
    protected $passwordDecorators = array(
        array(array('IconWrapper' => 'HtmlTag'), array('tag' => 'i', 'class' => 'icon-lock')),
        array(array('SpanWrapper' => 'HtmlTag'), array('tag' => 'span', 'class' => 'add-on')),
        array('ViewHelper', array('placement' => 'append')),
        array(array('DivWrapper' => 'HtmlTag'), array('tag' => 'div', 'class' => 'input-prepend', 'data-rel' => 'tooltip', 'data-original-title' => 'Password')),
        array(array('ClearFix' => 'HtmlTag'), array('tag' => 'div', 'class' => 'clear-fix', 'placement' => 'append')),
        'Errors'
    );
    
    protected $loginDecorators = array(
        array('Label'),
        array(array('Button' => 'HtmlTag'), array('tag' => 'button', 'class' => 'btn btn-primary', 'type' => 'submit')),
        array(array('Paragraph' => 'HtmlTag'), array('tag' => 'p', 'class' => 'center span5')),
        'Errors'
    );
    
    public function init()
    {
        // set the form method to POST
        $this->setMethod('post');
        
        $this->setDescription('Please login with your Username and Password.');
        
        $this->setAttrib('class', 'form-horizontal');
        
        $this->addDecorator('Description', array('tag' => 'div', 'class' => 'alert alert-info'))
             ->addDecorator('FormElements', array('placement' => 'append'))
             ->addDecorator('HtmlTag', array('tag' => 'fieldset'))
             ->addDecorator('Form');
        
        //add usenam element
        $this->addElement('text', 'username', array(
            'label' => 'Username:',
            'filters' => array('StringTrim'),
            'required' => true,
            'class' => 'input-large span10',
            'decorators' => $this->usernameDecorators
        ));
        
        //add element password
        $this->addElement('password', 'password', array(
            'label' => 'Password:',
            'required' => true,
            'class' => 'input-large span10',
            'validators' => array(array('StringLength', false, array('min' => self::PASSWORD_MIN_LENGHT))),
            'decorators' => $this->passwordDecorators
        ));
        
        // add element submit
        $this->addElement('submit', 'submit', 
                array(
                    'ignore' => true,
                    'label' => 'Login',
                    'decorators' => $this->loginDecorators
                ));
        
        // add CSRF protection
        $this->addElement('hash', 'crsf', 
                array(
                    'ignore' => true
                ));
    }
}
