<?php
/**
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Controllers
 * @copyright GAZE-ND
 */

/**
 * Login Controller
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Admin
 * @package Admin Controllers
 * @copyright GAZE-ND
 */
class Admin_LoginController extends Zend_Controller_Action
{
    /**
     * (non-PHPdoc)
     * @see Zend_Controller_Action::init()
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Creates the Login Form
     */
    public function indexAction()
    {
        $form = new Admin_Form_Login();
        $this->view->form = $form;
        
    }


}

