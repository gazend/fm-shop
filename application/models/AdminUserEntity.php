<?php
/**
 * FM Shop ©
 * Copyright © 2012 Georgi Ivanov <gopa_bg@abv.bg>
 *
 * LICENSE
 *
 * A copy of this license is bundled with this package in the file LICENSE.txt.
 *
 * Copyright © FM Shop
 *
 * Platform that uses this site is protected by copyright.
 * It is provided solely for the use of this site and all its copying,
 * processing or use of parts thereof is prohibited and pursued by law.
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */

/**
 * Admin User Entity class
 *
 * @author Georgi Ivanov <gopa_bg@abv.bg>
 * @category Application
 * @package Application Models
 * @copyright GAZE-ND
 */
class Application_Model_AdminUserEntity extends GZend_Entity_AbstractEntity
{
    
    /**
     * User ID
     * 
     * @var integer
     */
    protected $id;
    
    /**
     * Username
     *
     * @var string
     */
    protected $username;
    
    /**
     * Password
     *
     * @var string
     */    
    protected $password;
    
    /**
     * Email
     *
     * @var string
     */
    protected $email;
    
	/**
     * Getter for $id
     *
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

	/**
     * Setter for $id
     *
     * @param number $id
     * @return Application_Model_AdminUserEntity Provides fluent interface
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

	/**
     * Getter for $username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

	/**
     * Setter for $username
     *
     * @param string $username
     * @return Application_Model_AdminUserEntity Provides fluent interface
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

	/**
     * Getter for $password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

	/**
     * Setter for $password
     *
     * @param string $password
     * @return Application_Model_AdminUserEntity Provides fluent interface
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

	/**
     * Getter for $email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

	/**
     * Setter for $email
     *
     * @param string $email
     * @return Application_Model_AdminUserEntity Provides fluent interface
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

}